A Docker file that provides pdflatex

## REquirements
Install Docker for your operating system (e.g. on Debian https://docs.docker.com/install/linux/docker-ce/debian/)

## Building a Docker image for Latex
This will download all the required libraries and pack them in a Docker image.
The completed image will be called "pdflatex" and will occupy 2.7GB of disk space.
```
make
```
## Testing and running
To run the container, call
```
make run
```
To render the example Latex file to PDF, do:
```
make test
```
The example will be rendered to ```test/example.pdf```.
