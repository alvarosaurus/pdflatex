FROM debian:stretch-slim

LABEL Description="Image that provides pdflatex" Vendor="Alvaro Ortiz Troncoso" Version="0.2"

# Basic packages
RUN apt-get update \
    && apt-get -y install nano vim  \
    && apt-get install -y --no-install-recommends texlive-full

CMD sleep infinity
    

