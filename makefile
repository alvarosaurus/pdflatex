.PHONY: all run test stop remove
default: all

# Name of the container with Latex
LTX_CONTAINER=pdflatex

###########################################
#
# Build the image.
#
###########################################
all:
	docker build -f Dockerfile -t ${LTX_CONTAINER} .
	docker images 


#############################################
#
# Start the container.
#
#############################################
run:stop
	docker run \
		--restart no \
		--name ${LTX_CONTAINER} \
		-v $(shell pwd)/test:/test \
		-td \
		${LTX_CONTAINER}
	docker ps


#############################################
#
# Test the container.
#
#############################################
test:run
	-rm -f test/example.pdf
	docker exec -ti ${LTX_CONTAINER} script -q -c "cd /test && pdflatex example.tex"


#############################################
#
# Stop and remove the container.
#
#############################################
stop:
	-docker stop ${LTX_CONTAINER}
	-docker rm ${LTX_CONTAINER}


#############################################
#
# Remove the image and container.
#
#############################################
remove:stop
	-docker rmi ${LTX_CONTAINER}

